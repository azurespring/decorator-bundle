<?php

namespace AzureSpring\Bundle\SerializerDecoratorBundle;

use AzureSpring\Bundle\SerializerDecoratorBundle\Decorator\DecoratorInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class AzureSpringSerializerDecoratorBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import('../config/services.yaml');
        $builder
            ->registerForAutoconfiguration(DecoratorInterface::class)
            ->addTag('azurespring_serializer_decorator.decorator')
        ;
    }
}
