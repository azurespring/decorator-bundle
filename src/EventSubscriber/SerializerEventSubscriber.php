<?php

namespace AzureSpring\Bundle\SerializerDecoratorBundle\EventSubscriber;

use AzureSpring\Bundle\SerializerDecoratorBundle\Annotation\Decorator;
use AzureSpring\Bundle\SerializerDecoratorBundle\Decorator\DecoratorInterface;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\Metadata\StaticPropertyMetadata;
use Symfony\Component\DependencyInjection\ServiceLocator;

class SerializerEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => 'serializer.post_serialize',
                'method' => 'onPostSerialize',
                'format' => 'json',
            ],
        ];
    }

    public function __construct(private readonly ServiceLocator $serviceLocator)
    {
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $r = new \ReflectionClass($event->getObject());
        $attributes = $r->getAttributes(Decorator::class);
        if (!$attributes) {
            return;
        }

        /** @var Decorator $attribute */
        $attribute = $attributes[0]->newInstance();
        foreach ($attribute->value as $serviceId) {
            $decorator = $this->serviceLocator->get($serviceId);
            $metadata = new StaticPropertyMetadata('', '', $decorator);
            $metadata->inline = true;

            /** @var JsonSerializationVisitor $visitor */
            $visitor = $event->getVisitor();
            if (!$decorator instanceof DecoratorInterface) {
                $visitor->visitProperty($metadata, $decorator);
            } else {
                $decorator->accept($object = $event->getObject());
                $visitor->visitProperty($metadata, $decorator);
                $decorator->retire($object);
            }
        }
    }
}
