<?php

namespace AzureSpring\Bundle\SerializerDecoratorBundle\Annotation;

#[\Attribute(\Attribute::TARGET_CLASS)]
class Decorator
{
    public array $value;

    public function __construct(array|string $value)
    {
        $this->value = (array) $value;
    }
}
