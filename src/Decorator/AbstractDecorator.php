<?php

namespace AzureSpring\Bundle\SerializerDecoratorBundle\Decorator;

use JMS\Serializer\Annotation\ExclusionPolicy;

#[ExclusionPolicy('all')]
class AbstractDecorator implements DecoratorInterface
{
    protected $objects = [];

    public function accept($object): void
    {
        $this->objects[] = $object;
    }

    public function retire($object): void
    {
        $last = array_pop($this->objects);
        if ($last !== $object) {
            throw new \LogicException();
        }
    }

    protected function last(): mixed
    {
        return $this->objects[count($this->objects) - 1];
    }
}
