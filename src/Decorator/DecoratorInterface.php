<?php

namespace AzureSpring\Bundle\SerializerDecoratorBundle\Decorator;

interface DecoratorInterface
{
    public function accept($object): void;
    public function retire($object): void;
}
